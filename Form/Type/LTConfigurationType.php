<?php
/**
 * Created by Rubikin Team.
 * Date: 9/5/13
 * Time: 1:50 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\Form\Type;


use Nilead\ResourceBundle\Form\Type\AbstractResourceType;
use Nilead\ShipmentGHNBundle\Carrier;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LTConfigurationType extends AbstractResourceType
{
    protected $carrier;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('service', 'choice', array('choices' => Carrier::getRatesAvailable()));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(array(
                'data_class' => null,
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'nilead_shipment_ghn_lt_configuration';
    }
}
