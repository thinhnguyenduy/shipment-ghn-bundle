<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 2:00 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\tests\unit\Message;

use Nilead\ShipmentCommonBundle\tests\unit\TestCase;
use Nilead\ShipmentGHNBundle\Message\RatesResponse;
use Mockery as m;

class RatesResponseTest extends TestCase
{
    protected $request;

    public function testSuccess()
    {
        $httpResponse = $this->getMockHttpResponse('RatesSuccess.txt');

        $request = $this->getMockRatesRequest();

        $request->shouldReceive('getMethods')->once()->andReturn([
            1 => m::mock('\Nilead\ShipmentComponent\Model\ShippingMethodInterface')
                    ->shouldReceive('getName')->andReturn('name')->getMock()
                    ->shouldReceive('getId')->andReturn('id')->getMock()

        ]);

        $response = new RatesResponse($request, $httpResponse->json());

        $rates = $response->getRates();

        $this->assertTrue($response->isSuccessful());
        $this->assertEquals(10000, $rates[1]['total']);
    }

    public function testFailure()
    {
        $httpResponse = $this->getMockHttpResponse('RatesFailure.txt');

        $request = $this->getMockRatesRequest();

        $response = new RatesResponse($request, $httpResponse->json());

        $this->assertFalse($response->isSuccessful());
    }
}
