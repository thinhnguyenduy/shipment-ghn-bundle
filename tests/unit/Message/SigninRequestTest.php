<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 2:00 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\tests\unit\Message;

use Nilead\ShipmentCommonBundle\tests\unit\TestCase;
use Nilead\ShipmentGHNBundle\Message\SigninRequest;

class SigninRequestTest extends TestCase
{
    protected $request;

    public function setUp()
    {
        $this->request = new SigninRequest();
        $this->request->setServices($this->getHttpClient(), $this->getHttpRequest());
    }

    public function testGetData()
    {
        $this->request->setClientID(2619);
        $this->request->setPassword('JpTwK4tNkm1VENv8V');

        $this->assertEquals([
            "ClientID" => 2619,
            "Password" => "JpTwK4tNkm1VENv8V"
            ],
        $this->request->getData());
    }
}
