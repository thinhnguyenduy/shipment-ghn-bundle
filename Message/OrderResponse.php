<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 1:10 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\Message;

use Nilead\ShipmentCommonComponent\Message\OrderResponseInterface;
use Nilead\ShipmentCommonComponent\Message\RequestInterface;

class OrderResponse extends ResponseAbstract implements OrderResponseInterface
{
    protected $data;

    public function __construct(RequestInterface $request, $data)
    {
        $this->request = $request;
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function isSuccessful()
    {
        return $this->data['ResponseException'] == null;
    }

    /**
     * {@inheritdoc}
     */
    public function getTransactionReference()
    {
        return $this->data['OrderCode'];
    }

    /**
     * {@inheritdoc}
     */
    public function getTrackingId()
    {
        return $this->data['OrderCode'];
    }

    /**
     * {@inheritdoc}
     */
    public function getCost()
    {
        return $this->data['TotalFee'];
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderId()
    {
        return $this->data['OrderID'];
    }
}
